#pragma once
#include "base_kinematics.h"

namespace srrg_l2c{

  // Taylor expansion terms approximation
  const real EPSILON = 1e-6;

  // comment if you don't want to use the Taylor expansion
  // version of direct kinematics
#define _USE_TAYLOR_EXPANSION_
  
  class DifferentialDriveKinematics: public BaseKinematics{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;    

    const static int intrinsics_size = 3;
    // kl : i.e. left  wheel radius/speed ratio
    // kr : i.e. right wheel radius/speed ratio
    // baseline : wheels distance
    
    DifferentialDriveKinematics() {
      odom_params_.setZero(intrinsics_size);
    }
    
    ~DifferentialDriveKinematics(){
      std::cerr << FRED("[DifferentialDriveKinematics] destroyed\n");
    }

    void directKinematics(Vector3& pose,
                          const VectorX& encoder_ticks);
    void directKinematics(Vector3& pose,
                          const VectorX& encoder_ticks,
                          const VectorX& odom_params);
   
    void update(const VectorX& dx_) {
      odom_params_ += dx_;
    }

    const void print() const;
    
  };

  typedef std::shared_ptr<DifferentialDriveKinematics> DifferentialDriveKinematicsPtr;
  typedef std::shared_ptr<DifferentialDriveKinematics const> DifferentialDriveKinematicsConstPtr;

  
}
