#include "differential_drive_kinematics.h"

namespace srrg_l2c{

  template <typename T>
  inline T sinThetaOverTheta(T theta) {
    if (fabs(theta)<EPSILON)
      return 1;
    return sin(theta)/theta;
  }

  template <typename T>
  inline T oneMinisCosThetaOverTheta(T theta) {
    if (fabs(theta)<EPSILON)
      return 0;
    return (1.0f-cos(theta))/theta;
  }


  template <typename T>
  void computeThetaTerms(T& sin_theta_over_theta,
                         T& one_minus_cos_theta_over_theta,
                         T theta) {
#ifdef _USE_TAYLOR_EXPANSION_
    const T cos_coeffs[]={0., 0.5 ,  0.    ,   -1.0/24.0,   0.    , 1.0/720.0};
    const T sin_coeffs[]={1., 0.  , -1./6. ,      0.    ,   1./120, 0.   };
    // evaluates the taylor expansion of sin(x)/x and (1-cos(x))/x,
    // where the linearization point is x=0, and the functions are evaluated
    // in x=theta
    sin_theta_over_theta=0;
    one_minus_cos_theta_over_theta=0;
    T theta_acc=1;
    for (uint8_t i=0; i<6; i++) {
      if (i&0x1)
        one_minus_cos_theta_over_theta+=theta_acc*cos_coeffs[i];
      else
        sin_theta_over_theta+=theta_acc*sin_coeffs[i];
      theta_acc*=theta;
    }
#else
    sin_theta_over_theta=sinThetaOverTheta(theta);
    one_minus_cos_theta_over_theta=oneMinisCosThetaOverTheta(theta) ;
#endif
  }



  void DifferentialDriveKinematics::directKinematics(Vector3& pose,
                                                     const VectorX& encoder_ticks){
    directKinematics(pose, encoder_ticks, odom_params_);
  }

  void DifferentialDriveKinematics::directKinematics(Vector3& pose,
                                                     const VectorX& encoder_ticks,
                                                     const VectorX& odom_params){
    
    
    real delta_l = encoder_ticks(0)*odom_params(0);
    real delta_r = encoder_ticks(1)*odom_params(1);
    real baseline = odom_params(2);

    real delta_plus=delta_r+delta_l;
    real delta_minus=delta_r-delta_l;
    real dth=delta_minus/baseline;
    real one_minus_cos_theta_over_theta, sin_theta_over_theta;
    computeThetaTerms(sin_theta_over_theta, one_minus_cos_theta_over_theta, dth);
    real dx=.5*delta_plus*sin_theta_over_theta;
    real dy=.5*delta_plus*one_minus_cos_theta_over_theta;

    //apply the increment to the previous estimate
    real s=sin(pose(2));
    real c=cos(pose(2));
    pose(0) +=c*dx-s*dy;
    pose(1) +=s*dx+c*dy;
    pose(2) +=dth;
    srrg_core::normalizeAngle(pose(2));
  }

  const void DifferentialDriveKinematics::print() const {
    std::cerr << BOLD(FGRN("[DiffDrive ROBOT] ")) << std::endl
              << FGRN("  - [OdomParams] ")
              << odom_params_.transpose()
              << std::endl;      
  }

  
}
