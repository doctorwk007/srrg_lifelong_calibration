#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_utils/file_reader.h>
#include <srrg_l2c_solvers/solver.h>
#include <srrg_l2c_utils/trajectory_partitioner.h>
#include <srrg_l2c_types/matrix_info.h>
#include <srrg_l2c_kinematics/forklift_kinematics.h>


using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "offline_app: from nw to valhalla",
  "",
  "usage: offline_app_forklift -encoder encoder.txt -estimate sensor_est.txt",
  "-encoder  <string>       txt file containing encoder measurements [timestamp fwd_vel steering_angle]",
  "-estimate  <string>      txt file containing pose sensor estimate measures",
  "-guess_robot <Vector2>   initial guess in form [DistX DistY]",
  "-guess_sensor <Vector6>  initial guess in form [x y z qx qy qz]",
  "-h     <flag>            this help",
  0
};


int main(int argc, char** argv){

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  int c = 1;

  std::string encoder_file = "";
  std::string sensor_file  = "";

  Vector2 initial_guess_forklift(0.1, 0.1);
  Vector6 initial_guess_sensor = Vector6::Zero();

  bool estimate_time_delay = false;
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-encoder")) {
      c++;
      encoder_file = argv[c];
    } else if (!strcmp(argv[c], "-estimate")) {
      c++;
      sensor_file = argv[c];
    } else if (!strcmp(argv[c], "-guess_robot")) {
      c++;
      for(size_t i = 0; i < 2; ++i)
        initial_guess_forklift(i) = std::atof(argv[c++]);
    } else if (!strcmp(argv[c], "-guess_sensor")) {
      c++;
      for(size_t i = 0; i < 6; ++i)
        initial_guess_sensor(i) = std::atof(argv[c++]);
    }
    c++;
  }

  
  std::string sensor_name = "sensor";
  DatasetPtr encoder_dataset = DatasetPtr(new Dataset());
  Measure::Type& encoder_dataset_type = encoder_dataset->type();
  encoder_dataset_type = Measure::Type::Relative;
  
  DatasetPtr sensor_dataset = DatasetPtr(new Dataset());

  FileReader filereader;
  filereader.setFile(encoder_file);
  if(!filereader.computeForkliftEncoder(*encoder_dataset))
    throw std::runtime_error("[Main]: Error while processing encoder dataset");
  filereader.setFile(sensor_file);
  if(!filereader.compute(*sensor_dataset))
    throw std::runtime_error("[Main]: Error while processing sensor dataset");
   
  SensorDatasetMap* sensor_dataset_map = new SensorDatasetMap();
  sensor_dataset_map->insert(SensorDatasetPair(sensor_name, sensor_dataset));

  SensorPtr sensor1 = std::make_shared<Sensor>(sensor_name);
  sensor1->setExtrinsics(initial_guess_sensor);
  sensor1->addPrior(-0.322986, Sensor::Y);
  sensor1->addPrior(0.0, Sensor::Z);

  
  ForkliftKinematicsPtr forklift = std::make_shared<ForkliftKinematics>();
  forklift->setOdomParams(initial_guess_forklift);
  
  Solver solver;
  
  solver.setKinematics(forklift);  
  solver.setSensor(sensor1);
  const int iterations = 20; 
  solver.setIterations(iterations);
  solver.setEpsilon(1e-4);
  solver.setEpsilonTime(6e-2);
  
  solver.init();
  
  solver.compute(encoder_dataset, *sensor_dataset_map);
  solver.stats().print();

  forklift->print();
  sensor1->print();
  
  delete sensor_dataset_map;
  
  return 0;
}
