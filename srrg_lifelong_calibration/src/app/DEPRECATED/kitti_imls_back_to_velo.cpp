#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_system_utils/colors.h>
#include <srrg_l2c_utils/file_reader.h>

using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "kitti_imls_back_to_velo: move back imls trajectory to velo",
  "",
  "usage: kitti_imls_back_to_velo <input-imls> <velo_wrt_cam> <output-imls>",
  "-h <flag>               this help",
  0
};


Isometry3 getTransform(const std::string& filename) {
  std::ifstream file;
  file.open(filename);
  if(!file.is_open()) {
    std::cerr << KRED << "unable to read file: " << filename
              << RST << std::endl;      
    std::exit(0);
  }
  Matrix3 R = Matrix3::Zero();
  Vector3 t = Vector3::Zero();
  file >> R(0,0) >> R(0,1) >> R(0,2) >> t(0)
       >> R(1,0) >> R(1,1) >> R(1,2) >> t(1)
       >> R(2,0) >> R(2,1) >> R(2,2) >> t(2);

  Isometry3 T;
  T.linear() = R;
  T.translation() = t;
  return T;
}

int main(int argc, char** argv){
  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 1;
  }

  int c = 1;

  std::string input_file    = "";
  std::string velo2cam_file = "";
  std::string output_file   = "";

  while (c < argc) {
    if(!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    }
    input_file = argv[c++];
    velo2cam_file = argv[c++];
    output_file = argv[c++];
  }

  if(input_file.empty())
    throw std::runtime_error("Missing input file!");
  if(velo2cam_file.empty())
    throw std::runtime_error("Missing velo2cam file!");
  if(output_file.empty())
    throw std::runtime_error("Missing output file!");

  DatasetPtr input_dataset = DatasetPtr(new Dataset());
  FileReader filereader;
  filereader.setFile(input_file);
  if(!filereader.compute12(*input_dataset))
    throw std::runtime_error("[Main]: Error while processing input dataset!");

  Isometry3 velo_WRT_cam = getTransform(velo2cam_file);
  Isometry3 cam_WRT_velo = velo_WRT_cam.inverse();
  

  // transform dataset to represent the IMU poses
  input_dataset->transformInPlace(cam_WRT_velo.inverse());
  input_dataset->write12(output_file);
  
  return 0;
}
