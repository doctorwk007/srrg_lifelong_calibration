#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_utils/file_reader.h>
#include <srrg_l2c_solvers/solver.h>
#include <srrg_l2c_utils/trajectory_partitioner.h>
#include <srrg_l2c_utils/trajectory_splitter.h>
#include <srrg_l2c_types/matrix_info.h>

using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "offline_app_kitti: offline app designed for kitti-like data",
  "",
  "usage: offline_app_kitti -odom odom.txt -estimate sensor_est.txt",
  "-odom       <string>    txt file containing odom measurements",
  "-estimate   <string>    txt file containing pose sensor estimate measures",
  "-gt         <string>    txt file containing the actual param",
  "-h          <flag>      this help",
  0
};

Isometry3 getTransform(const std::string& filename) {
  std::ifstream file;
  file.open(filename);
  if(!file.is_open()) {
    std::cerr << KRED << "unable to read file: " << filename
              << RST << std::endl;      
    std::exit(0);
  }
  Matrix3 R = Matrix3::Zero();
  Vector3 t = Vector3::Zero();
  file >> R(0,0) >> R(0,1) >> R(0,2) >> t(0)
       >> R(1,0) >> R(1,1) >> R(1,2) >> t(1)
       >> R(2,0) >> R(2,1) >> R(2,2) >> t(2);

  Isometry3 T;
  T.linear() = R;
  T.translation() = t;
  return T;
}

int main(int argc, char** argv){

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 0;
  }

  int c = 1;

  std::string odom_file   = "";
  std::string sensor_file = "";
  std::string actual_param = "";
  Vector6 initial_guess;
  initial_guess.setZero();
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-odom")) {
      c++;
      odom_file = argv[c];
    } else if (!strcmp(argv[c], "-estimate")) {
      c++;
      sensor_file = argv[c];
    } else if (!strcmp(argv[c], "-gt")) {
      c++;
      actual_param = argv[c];
    }
    c++;
  }
  
  std::string sensor_name = "sensor";
  DatasetPtr odometry_dataset = DatasetPtr(new Dataset());
  DatasetPtr sensor_dataset = DatasetPtr(new Dataset());

  FileReader filereader;
  filereader.setFile(odom_file);
  if(!filereader.compute12(*odometry_dataset))
    throw std::runtime_error("[Main]: Error while processing odometry dataset");
  filereader.setFile(sensor_file);
  if(!filereader.compute12(*sensor_dataset))
    throw std::runtime_error("[Main]: Error while processing sensor dataset");
  
  // Analyze the dataset
  TrajectoryAnalyzer trajectory_analyzer;
  trajectory_analyzer.compute(*odometry_dataset);
  
  SensorDatasetMap*  sensor_dataset_map = new SensorDatasetMap();
  sensor_dataset_map->insert(SensorDatasetPair(sensor_name, sensor_dataset));

  Sensor sensor1(sensor_name);
  Vector6 good_guess;
  good_guess << 0.9, -0.2, 0.7, -0.5, 0.5,-0.5;
  sensor1.setExtrinsics(good_guess);
  sensor1.addPrior(0.746412, Sensor::Z);
    
  
  TrajectorySplitter trajectory_splitter;
  trajectory_splitter.setDataset(sensor_dataset);
  DatasetVector dataset_vector;
  trajectory_splitter.compute(dataset_vector);
  std::cerr << FYEL("Dataset Vector size: ") << dataset_vector.size() << std::endl;
  
  
  //  std::map<real, DatasetPtr> scored_dataset_map;
  std::vector<std::pair<MatrixX, DatasetPtr> > scored_dataset_vector;
  scored_dataset_vector.resize(dataset_vector.size());
  std::vector<bool> visited(dataset_vector.size(), false);
  
  for(size_t i = 0; i < dataset_vector.size(); ++i) {
    sensor1.setExtrinsics(good_guess);
    Solver tmp_solver;
    tmp_solver.setSensor(&sensor1);
    tmp_solver.setIterations(10);
    tmp_solver.setEpsilon(1e-4);
    
    tmp_solver.init();
    SensorDatasetMap* portion = new SensorDatasetMap();
    portion->insert(SensorDatasetPair(sensor_name, dataset_vector[i]));
    tmp_solver.compute(odometry_dataset, *portion);
    delete portion;
    
    //    const real determinant = tmp_solver.H().determinant();
    scored_dataset_vector[i] = std::pair<MatrixX, DatasetPtr>(tmp_solver.H(), dataset_vector[i]);
  }

  const int k = 2 * scored_dataset_vector.size() / 3;
  std::cerr << BOLD(FYEL("k: ")) << k << std::endl;
  
  sensor1.setExtrinsics(good_guess);

  Solver solver;
  solver.setSensor(&sensor1);
  solver.setIterations(10);
  solver.setEpsilon(1e-4);    
  solver.init();

  float determinant = 0;
  float best_eig_ratio = 0;
  int best_eig_ratio_index = -1;

  int portions_used = 0;
  
  for(size_t ik = 0; ik < k; ++ik) {
    for(size_t i = 0; i < scored_dataset_vector.size(); ++i) {
      if(visited[i])
        continue;
      const MatrixX& portion_H = scored_dataset_vector[i].first;
      const MatrixX predicted_H = portion_H + solver.H();
      MatrixInfo matrix_info(predicted_H);
      matrix_info.compute();
      if(matrix_info.eig_ratio > best_eig_ratio &&
         matrix_info.determinant > determinant) {
        best_eig_ratio = matrix_info.eig_ratio;
        best_eig_ratio_index = i;
      }      
    }
    if(best_eig_ratio_index == -1)
      break;

    SensorDatasetMap* portion = new SensorDatasetMap();
    portion->insert(SensorDatasetPair(sensor_name, scored_dataset_vector[best_eig_ratio_index].second));
    solver.compute(odometry_dataset, *portion);
    delete portion;
    visited[best_eig_ratio_index] = true;
    MatrixInfo current_H_info(solver.H());
    current_H_info.compute();
    best_eig_ratio_index = -1;
    best_eig_ratio = current_H_info.eig_ratio;
    determinant = current_H_info.determinant;
    portions_used++;
  }

  std::cerr << FYEL("portions actually used: ") << portions_used << std::endl;

  if(!actual_param.empty()) {
    // compute estimation error
    const Isometry3 actual_extrinsics =  getTransform(actual_param);
    const Isometry3 extrinsics_error = actual_extrinsics.inverse() * sensor1.extrinsics();
    sensor1.print();
    std::cerr << FRED(" err_t: ") << extrinsics_error.translation().norm()
              << FRED(" err_r: ") << computeAngle(extrinsics_error.linear()) << std::endl;
  }


  delete sensor_dataset_map;  
  return 0;
}
