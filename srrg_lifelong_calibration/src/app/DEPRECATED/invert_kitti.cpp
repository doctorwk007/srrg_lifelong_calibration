#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_system_utils/colors.h>
#include <srrg_l2c_utils/file_reader.h>

using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "invert_kitti_transform: read, invert, save a kitti transform",
  "",
  "usage: invert_kitti_transform <kitti-input-file> <kitti-output-file>",
  "-h <flag>               this help",
  0
};


Isometry3 getTransform(const std::string& filename) {
  std::ifstream file;
  file.open(filename);
  if(!file.is_open()) {
    std::cerr << KRED << "unable to read file: " << filename
              << RST << std::endl;      
    std::exit(0);
  }
  Matrix3 R = Matrix3::Zero();
  Vector3 t = Vector3::Zero();
  file >> R(0,0) >> R(0,1) >> R(0,2) >> t(0)
       >> R(1,0) >> R(1,1) >> R(1,2) >> t(1)
       >> R(2,0) >> R(2,1) >> R(2,2) >> t(2);

  Isometry3 T;
  T.linear() = R;
  T.translation() = t;
  return T;
}

int main(int argc, char** argv){
  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 1;
  }

  int c = 1;

  std::string input_file  = "";
  std::string output_file = "";

  while (c < argc) {
    if(!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    }
    input_file  = argv[c++];
    output_file = argv[c++];
  }

  if(input_file.empty())
    throw std::runtime_error("Missing input file!");
  if(output_file.empty())
    throw std::runtime_error("Missing output file!");

  Isometry3 input_T = getTransform(input_file);
  Isometry3 output_T = input_T.inverse();
  Matrix4 output_M = output_T.matrix();
  
  std::ofstream output_stream;
  output_stream.open(output_file);
  for(int r = 0; r < 3; ++r)
    for(int c = 0; c < 4; ++c)
      output_stream << output_M(r,c) << " ";

  output_stream.close();
  return 0;
}
