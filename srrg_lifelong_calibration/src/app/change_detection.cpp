#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_utils/file_reader.h>
#include <srrg_l2c_utils/change_detector.h>
#include <srrg_l2c_solvers/solver.h>
#include <srrg_l2c_utils/trajectory_partitioner.h>
#include <srrg_l2c_utils/trajectory_splitter.h>
#include <srrg_l2c_types/matrix_info.h>

using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "offline_app_kitti: offline app designed for kitti-like data",
  "",
  "usage: offline_app_kitti -odom odom.txt -estimate sensor_est.txt",
  "-odom       <string>    txt file containing odom measurements",
  "-estimate   <string>    txt file containing pose sensor estimate measures",
  "-guess      <Vector6>   initial guess in form [x y z qx qy qz]",
  "-time_delay <flag>      turn on time delay estimate.",
  "-gt         <string>    txt file containing the actual param",
  "-o          <flag>      generate output file with error wrt gt",
  "-h          <flag>      this help",
  "",
  "note: multiple sensors are supported, e.g. -estimate stereo.txt -estimate velodyne.txt -gt stereo_param.txt -gt velo_param.txt",
  0
};

Isometry3 getTransform(const std::string& filename) {
  std::ifstream file;
  file.open(filename);
  if(!file.is_open()) {
    std::cerr << KRED << "unable to read file: " << filename
              << RST << std::endl;      
    std::exit(0);
  }
  Matrix3 R = Matrix3::Zero();
  Vector3 t = Vector3::Zero();
  file >> R(0,0) >> R(0,1) >> R(0,2) >> t(0)
       >> R(1,0) >> R(1,1) >> R(1,2) >> t(1)
       >> R(2,0) >> R(2,1) >> R(2,2) >> t(2);

  Isometry3 T;
  T.linear() = R;
  T.translation() = t;
  return T;
}

int main(int argc, char** argv){

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 0;
  }

  int c = 1;

  std::string odom_file   = "";
  std::vector<std::string> sensor_files;
  std::vector<std::string> actual_params;
  Vector6Vector initial_guesses;
  bool estimate_time_delay = false;
  bool output_results = false;
  
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-odom")) {
      c++;
      odom_file = argv[c];
    } else if (!strcmp(argv[c], "-estimate")) {
      c++;
      sensor_files.push_back(argv[c]);
    } else if (!strcmp(argv[c], "-gt")) {
      c++;
      actual_params.push_back(argv[c]);
    } else if (!strcmp(argv[c], "-time_delay")) {
      estimate_time_delay = true;
    } else if (!strcmp(argv[c], "-o")) {
      output_results = true;
    } else if (!strcmp(argv[c], "-guess")) {
      c++;
      Vector6 current_guess(Vector6::Zero());
      for(size_t i = 0; i < 6; ++i)
        current_guess(i) = std::atof(argv[c++]);
      initial_guesses.push_back(current_guess);
    }
    c++;
  }

  const int number_of_sensor = sensor_files.size();
  std::cerr << FGRN("[Main] Calibrating ") << number_of_sensor
            << FGRN(" sensors") << std::endl;
  
  std::vector<std::string> sensor_names(number_of_sensor, "sensor_");
  for(int i = 0; i < number_of_sensor; ++i)
    sensor_names[i] += std::to_string(i);

  DatasetPtr odometry_dataset = DatasetPtr(new Dataset());
  SensorDatasetMap sensor_datasets;
  for(size_t i = 0; i < number_of_sensor; ++i)    
    sensor_datasets.insert(std::make_pair(sensor_names[i], DatasetPtr(new Dataset())));

  
  FileReader filereader;
  filereader.setFile(odom_file);
  if(!filereader.compute(*odometry_dataset))
    throw std::runtime_error("[Main]: Error while processing odometry dataset");
  for(size_t i = 0; i < number_of_sensor; ++i) {
    filereader.setFile(sensor_files[i]);
    if(!filereader.compute(*sensor_datasets.at(sensor_names[i])))
      throw std::runtime_error("[Main]: Error while processing sensor dataset");    
  }
  
  // Analyze the dataset
  TrajectoryAnalyzer trajectory_analyzer;
  trajectory_analyzer.compute(*odometry_dataset);
  
  std::vector<SensorPtr> sensors;
  for(size_t i = 0; i < number_of_sensor; ++i) {
    SensorPtr ith_sensor = std::make_shared<Sensor>(sensor_names[i]);
    sensors.push_back(ith_sensor);
    if(i < initial_guesses.size())
      sensors[i]->setExtrinsics(initial_guesses[i]);
    sensors[i]->addPrior(0.0, Sensor::Z);
    // if(i == 0)
    //   sensors[i]->addPrior(0.746412, Sensor::Z);  //stereo Z-gt
    // else if(i == 1)
    //   sensors[i]->addPrior(0.802724, Sensor::Z); //velo Z-gt
    if(estimate_time_delay)
      sensors[i]->setEstimateTimeDelay(true);
  }

  std::cerr << "Sensors initialized" << std::endl;
  Solver solver;
  for(size_t i = 0; i < number_of_sensor; ++i)
    solver.setSensor(sensors[i]);
  const int iterations = 150; 
  solver.setIterations(iterations);
  solver.setEpsilon(1e-4);
  solver.setEpsilonTime(1e-1);
  solver.setVerbosity(true);
  
  solver.init();

  std::cerr << "Solver initialized" << std::endl;
  
  solver.compute(odometry_dataset, sensor_datasets);
  solver.stats().print();
  std::cerr << "Solver done" << std::endl;
  
  for(size_t i = 0; i < number_of_sensor; ++i)
    sensors[i]->print();

  std::ofstream output_file;
  if(output_file)
    output_file.open("output_base.txt");
  
  for(size_t i = 0; i < actual_params.size(); ++i) {
    // compute estimation error
    const Isometry3 actual_extrinsics =  getTransform(actual_params[i]);
    const Isometry3 extrinsics_error = actual_extrinsics.inverse() * sensors[i]->extrinsics();
    std::cerr << KRED << sensor_names[i] << RST << std::endl;
    std::cerr << FRED("gt [extrinsics]: ") << srrg_core::t2v(actual_extrinsics).transpose() << std::endl;
    const double error_t = extrinsics_error.translation().norm();
    const double error_r = computeAngle(extrinsics_error.linear());
    std::cerr << FRED(" err_t: ") << error_t
              << FRED(" err_r: ") << error_r << std::endl;
    output_file << error_t << " " << error_r << std::endl;    
  }

  if(output_file)
    output_file.close();


  int samples_per_dataset = 200;
  double data_frequency = 0.1;
  const int sample_num = 3000;
  
  // once calibration is over on the whole dataset, we have our initial mean and covariance
  SensorDatasetMap odom_dataset_map;
  odom_dataset_map.insert(std::make_pair("odom_data", odometry_dataset));
  TrajectorySplitter trajectory_splitter(TrajectorySplitter::SampleNum);
  trajectory_splitter.mutableConfig().sample_per_dataset = samples_per_dataset;
  trajectory_splitter.setDataset(odom_dataset_map);
  SensorDatasetMapVector dataset_vector;
  trajectory_splitter.compute(dataset_vector);
  std::cerr << "Dataset Splitted in " << dataset_vector.size() << " portions"
            << std::endl;

  // apply a synthetic change in couple of parameters (move the camera and/or the velodyne) from a certain time or number of samples
  double time = sample_num * data_frequency; //kitti framerate
  double first_data_time = 0;
  Vector6 perturbation;
  perturbation << 0, .1, 0, .05, .05, 0;
  Isometry3 T_perturbation = srrg_core::v2t(perturbation);
  auto current_dataset_it = sensor_datasets.find("sensor_0");
  DatasetPtr& current_dataset = current_dataset_it->second;
  int sample_id = 0;
  for(const Sample& sample : *current_dataset) {
    if(sample_id == 0){
      first_data_time = sample.first;
      time += first_data_time;
    }
    ++sample_id;
    if(sample.first < time) {
      continue;
    }
    sample.second->transformInPlace(T_perturbation);

  }

  const real informativeness_threshold = 1e7;
  const real divergence_threshold = 1e-2;
    
  ChangeDetector change_detector;
  std::vector<Vector6> sensor_extrinsics;
  // backup sensor extrinsics
  for(const SensorPtr& sensor : sensors) {
    sensor->setInformation(1000);
    sensor_extrinsics.push_back(sensor->extrinsicsVector());
    change_detector.setSensor(sensor);    
  }
  
  const std::vector<int>& inliers = solver.stats().inliers_evolution;

  MatrixInfo first_H(solver.H());
  first_H.compute();
  
  const real first_informativeness = first_H.determinant*first_H.eig_ratio / inliers[inliers.size()-1];
  std::cerr << "First Informativeness: " << first_informativeness << std::endl;
  
  change_detector.setCovariance(solver.H().inverse());
  change_detector.init();
  
  for(size_t i = 0; i < dataset_vector.size(); ++i) {
    int sensor_id = 0;
    for(const SensorPtr& sensor : sensors) {
      sensor->setExtrinsics(sensor_extrinsics[sensor_id++]);
    }
    std::cerr << FBLU("\nPortion # ") << i << std::endl;
    solver.compute(dataset_vector[i].find("odom_data")->second, sensor_datasets);
    MatrixInfo current_H(solver.H());
    current_H.compute();
    const real informativeness = current_H.determinant*current_H.eig_ratio/ inliers[inliers.size()-1];
    std::cerr << "Informativeness: " << informativeness << std::endl;
    if(informativeness < informativeness_threshold)
      continue;
    sensors[0]->print();
    real kl_divergence = 0;
    change_detector.compute(kl_divergence, solver.H().inverse());
    std::cerr << "KL Divergence: " << kl_divergence << std::endl;
    if(kl_divergence > divergence_threshold) {
      std::cerr << FRED("Change Detected at time: ") << (i+1)*samples_per_dataset*data_frequency << std::endl;
      break;
    }
  }

  // reset sensor initial guess
  int sensor_id = 0;
  for(const SensorPtr& sensor : sensors) {
    sensor->setExtrinsics(sensor_extrinsics[sensor_id++]);
  }
  
  std::cerr << "*** Adaptive ***" << std::endl;
  
  // try with adaptive approach
  double portion_time = samples_per_dataset*data_frequency; // ~ seconds
  const double portion_time_increment = portion_time / 5.0;
  const double max_portion_time = portion_time * 1.5 - 0.5;
  double current_time = first_data_time;
  bool dataset_over = false;
  while(!dataset_over) {
    Dataset current_portion;
    if(!TrajectorySplitter::getDatasetPortion(current_portion,
                                              *odometry_dataset,
                                              current_time,
                                              current_time+portion_time)) {
      break;
    }
   
    solver.compute(std::make_shared<Dataset>(current_portion), sensor_datasets);
    MatrixInfo current_H(solver.H());
    current_H.compute();
    const real informativeness = current_H.determinant*current_H.eig_ratio/ inliers[inliers.size()-1];
    std::cerr << "Informativeness: " << informativeness  << " portion size: " << portion_time<< " [t: " << current_time-first_data_time << "|" << current_time-first_data_time+portion_time << " ]" <<std::endl;
    if(informativeness < informativeness_threshold) {
      if(portion_time < max_portion_time) {
        portion_time += portion_time_increment;      
        continue;
      }
      current_time += portion_time/2;
      continue;
    } else if(portion_time > samples_per_dataset*data_frequency) {
      portion_time = samples_per_dataset*data_frequency;
    }
    current_time += portion_time;
    sensors[0]->print();
    real kl_divergence = 0;
    change_detector.compute(kl_divergence, solver.H().inverse());
    std::cerr << "KL Divergence: " << kl_divergence << std::endl;
    if(kl_divergence > divergence_threshold) {
      std::cerr << FRED("Change Detected at time: ") << current_time - first_data_time << std::endl;
      break;
    }    
    
  }
  std::cerr<<"actual change applied at time: " << time - first_data_time << std::endl;
  
  return 0;
}
