#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_utils/file_reader.h>
#include <srrg_l2c_solvers/solver.h>
#include <srrg_l2c_utils/trajectory_partitioner.h>
#include <srrg_l2c_types/matrix_info.h>
#include <srrg_l2c_kinematics/forklift_kinematics.h>


using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "test_forklift: every time a new robot is added, it is good practice to perform synthetic tests on its kinematics",
  "",
  "usage: test_forklift_kinematics ",
  "-h     <flag>            this help",
  0
};


int main(int argc, char** argv){

  // bdc, parameters reading
  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    }
    c++;
  }

  // bdc, define the actual parameters of our forklift
  // we will use this parameter to generate a fake dataset
  Vector2 actual_forklift_params(0.68, 0.1);
  // bdc, same for a sensor mounter on the forklift
  // we define them as: x y z qx qy qz
  Vector6 sensor_v;
  sensor_v << 0.1, 0.2, 0.1, -0.5, 0.5 ,-0.5;
  Isometry3 sensor_T;
  sensor_T = srrg_core::v2t(sensor_v);

  // bdc, here is our forklift
  ForkliftKinematicsPtr forklift = ForkliftKinematicsPtr(new ForkliftKinematics());
  // bdc, set its parameters
  forklift->setOdomParams(actual_forklift_params);
  // bdc, and create an initial pose (x y yaw)
  Vector3 robot_pose(Vector3::Zero());
  
  // bdc, every sensor has to have a name
  std::string sensor_name = "Fake-Sensor";

  // bdc, now we can generate the fake dataset
  DatasetPtr encoder_dataset = DatasetPtr(new Dataset());
  DatasetPtr sensor_dataset = DatasetPtr(new Dataset());
  // bdc, the encoder dataset, in case of the forklift, is composed
  // by relative measures (this is not done by default)
  Measure::Type& encoder_dataset_type = encoder_dataset->type();
  encoder_dataset_type = Measure::Type::Relative;
  // bdc, parameters of our fake dataset
  const int dataset_size = 1000;
  double timestamp = 0.0;
  const double time_step = 0.05;
  const float fwd_vel_max = .1;
  float steer_angle = 0.0;
  // bdc, generate fake dataset
  for(size_t i = 0; i < dataset_size; ++i) {
    float fwd_vel = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/fwd_vel_max));
    if(steer_angle < 1.57)
      steer_angle += .06;
    else
      steer_angle -= .06;
    Vector2 measure(fwd_vel, steer_angle);    
    encoder_dataset->insert(Sample(timestamp, MeasurePtr(new Measure(measure, Measure::Relative))));
    forklift->directKinematics(robot_pose,
                               measure);
    Isometry3 iso_measure;
    iso_measure.setIdentity();
    iso_measure.translation().head(2) = robot_pose.head(2);
    iso_measure.linear() = srrg_core::Rz(robot_pose(2));
    iso_measure = iso_measure * sensor_T;
    sensor_dataset->insert(Sample(timestamp, MeasurePtr(new Measure(iso_measure))));    
    timestamp += time_step;
  }
  // bdc, we have to insert our sensor dataset in a map, since the solver is able to
  // calibrate simultaneously more than one sensor
  SensorDatasetMapPtr sensor_dataset_map = SensorDatasetMapPtr(new SensorDatasetMap());
  sensor_dataset_map->insert(SensorDatasetPair(sensor_name, sensor_dataset));

  // bdc, create a sensor. It has to have the same name of the dataset with which it is associated
  // so the fake one we have generated
  SensorPtr sensor1 = SensorPtr(new Sensor(sensor_name));  
  // bdc, set a Zero initial guess for the sensor extrinsics
  sensor1->setExtrinsics(Vector6::Zero());
  // bdc, add a prior on Z coord of the robot since it is unobservable
  sensor1->addPrior(0.1, Sensor::Z);
  // bdc, the two parameters, i.e. sensor Y coordinate and forklift wDy value
  // are correlated. The only thing that can be defined, given the kinematics of the robot,
  // is the relative difference between the two. So we have to fix one of the two
  sensor1->addPrior(0.2, Sensor::Y);
  // bdc, with velocity measure instead of encoder counts
  // (as in the forklift) the time delay estimate is impossible.
  // It has to be done starting from odometry
  sensor1->setEstimateTimeDelay(false);  

  // bdc, set a wrong but decent initial guess for the forklift intrinsics
  forklift->setOdomParams(Vector2(0.4, 0.3));  

  // bdc, a wild Solver is born
  Solver solver;
  // bdc, set the pointer to the forklift
  // it is used to access the kinematic model, to read and update the params
  solver.setKinematics(forklift);  
  // bdc, set the pointer to the sensor
  solver.setSensor(sensor1);
  // bdc, set max number of iteration for least squares solver
  solver.setIterations(10);
  // bdc, optional, set epsilon for numerical jacobian computation
  solver.setEpsilon(1e-4);
  // bdc, we want our solver to be verbose
  solver.setVerbosity(true);

  // bdc, init the solver given robot/sensor/params
  solver.init();
  
  // bdc, compute the calibration given two datasets (encoder/odom and sensor)
  // the sensor dataset is a map since it is possible to calibrate more than one sensor
  solver.compute(encoder_dataset, *sensor_dataset_map);

  // bdc, print some statistics from the solver
  solver.stats().print();

  // bdc, print the estimated parameters
  forklift->print();
  sensor1->print();
  
  return 0;
}
