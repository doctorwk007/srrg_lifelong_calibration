#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_utils/file_reader.h>
#include <srrg_l2c_solvers/solver.h>
#include <srrg_l2c_utils/trajectory_partitioner.h>
#include <srrg_l2c_types/matrix_info.h>
#include <srrg_l2c_kinematics/youbot_kinematics.h>


using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "test_youbot_kinematics: every time a new robot is added, it is good practice to perform synthetic tests on its kinematics",
  "",
  "usage: test_youbot_kinematics ",
  "-h     <flag>            this help",
  0
};


int main(int argc, char** argv){
  
  // bdc, parameters reading
  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    }
    c++;
  }

  // bdc, define the actual parameters of our differential drive
  // we will use this parameter to generate a fake dataset
  Vector5 actual_youbot_params;
  actual_youbot_params << 0.4, 0.12, -0.12, 0.12, -0.12;
  // bdc, same for a sensor mounter on the robot
  // we define them as: x y z qx qy qz
  Vector6 sensor_v;
  sensor_v << 0.1, 0.2, 0.1, -0.5, 0.5 ,-0.5;
  Isometry3 sensor_T;
  sensor_T = srrg_core::v2t(sensor_v);

  // bdc, here is our forklift
  YoubotKinematicsPtr youbot = YoubotKinematicsPtr(new YoubotKinematics());
  // bdc, set its parameters
  youbot->setOdomParams(actual_youbot_params);
  // bdc, and create an initial pose (x y yaw)
  Vector3 robot_pose(Vector3::Zero());
  
  // bdc, every sensor has to have a name
  std::string sensor_name = "Fake-Sensor";

  // bdc, now we can generate the fake dataset
  DatasetPtr encoder_dataset = DatasetPtr(new Dataset());
  DatasetPtr sensor_dataset = DatasetPtr(new Dataset());
  // bdc, parameters of our fake dataset
  const int dataset_size = 2000;
  double timestamp = 0.0;
  const double time_step = 0.05;
  const float max_increment = 0.1;
  float left_ticks = 0;
  float right_ticks = 0;
  float rear_left_ticks = 0;
  float rear_right_ticks = 0;  
  // bdc, generate fake dataset
  Vector4 old_measure(left_ticks, right_ticks, rear_left_ticks, rear_right_ticks);
  float left_increment = 0.1;
  float right_increment = -0.1;
  float rear_left_increment = -0.1;
  float rear_right_increment = 0.1;
  
  for(size_t i = 0; i < dataset_size; ++i) {
    if(i%10 == 0) {
      left_increment = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_increment));
      right_increment = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_increment));
      rear_left_increment = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_increment));
      rear_right_increment = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_increment));
      
    }
    left_ticks -= left_increment;
    right_ticks -= right_increment;
    rear_left_ticks -= rear_left_increment;
    rear_right_ticks -= rear_right_increment;

    normalizeAngle(left_ticks);
    normalizeAngle(right_ticks);
    normalizeAngle(rear_right_ticks);
    normalizeAngle(rear_left_ticks);
    const Vector4 measure(left_ticks, right_ticks, rear_left_ticks, rear_right_ticks);    

    encoder_dataset->insert(Sample(timestamp, MeasurePtr(new Measure(measure))));

    Vector4 relative_measure = measure - old_measure;
    old_measure = measure;
    normalizeAngle(relative_measure(0));
    normalizeAngle(relative_measure(1));
    normalizeAngle(relative_measure(2));
    normalizeAngle(relative_measure(3));
    youbot->directKinematics(robot_pose, relative_measure);
    
    Isometry3 iso_measure;
    iso_measure.setIdentity();
    iso_measure.translation().head(2) = robot_pose.head(2);
    iso_measure.linear() = srrg_core::Rz(robot_pose(2));
    iso_measure = iso_measure * sensor_T;
    sensor_dataset->insert(Sample(timestamp, MeasurePtr(new Measure(iso_measure))));    
    timestamp += time_step;
  }
  // bdc, we have to insert our sensor dataset in a map, since the solver is able to
  // calibrate simultaneously more than one sensor
  SensorDatasetMap* sensor_dataset_map = new SensorDatasetMap();
  sensor_dataset_map->insert(SensorDatasetPair(sensor_name, sensor_dataset));

  // bdc, create a sensor. It has to have the same name of the dataset with which it is associated
  // so the fake one we have generated
  SensorPtr sensor1 = std::make_shared<Sensor>(sensor_name);  
  // bdc, set a perturbated initial guess for the sensor extrinsics
  Vector6 pert;
  pert << -0.1, -0.2, 0.1, 0.1, 0.2 ,-0.3;
  sensor1->setExtrinsics(sensor_T  * srrg_core::v2t(pert));  // bdc, add a prior on Z coord of the robot since it is unobservable
  sensor1->addPrior(0.1, Sensor::Z);
  sensor1->setEstimateTimeDelay(false);
  

  // bdc, set a wrong but decent initial guess for the youbot intrinsics
  Vector5 guess_youbot_params;
  guess_youbot_params << 0.3, 0.1, -0.2, 0.09, -0.1;
  youbot->setOdomParams(guess_youbot_params);  

  // bdc, print initial status of robot and sensor params
  youbot->print();
  sensor1->print();
  
  // bdc, a wild Solver is born
  Solver solver;
  // bdc, set the pointer to the youbot
  // it is used to access the kinematic model, to read and update the params
  solver.setKinematics(youbot);  
  // bdc, set the pointer to the sensor
  solver.setSensor(sensor1);
  // bdc, set max number of iteration for least squares solver
  solver.setIterations(30);
  // bdc, optional, set epsilon for numerical jacobian computation
  solver.setEpsilon(1e-4);
  // bdc, we want our solver to be verbose
  solver.setVerbosity(true);

  // bdc, init the solver given robot/sensor/params
  solver.init();
  
  // bdc, compute the calibration given two datasets (encoder/odom and sensor)
  // the sensor dataset is a map since it is possible to calibrate more than one sensor
  solver.compute(encoder_dataset, *sensor_dataset_map);

  // bdc, print some statistics from the solver
  solver.stats().print();

  // bdc, print the estimated parameters
  youbot->print();
  sensor1->print();

  delete sensor_dataset_map;

  return 0;
}
