#include <iostream>

#include <srrg_l2c_solvers/solver.h>
#include "test_utilities.hpp"

using namespace srrg_l2c;
using namespace srrg_core;


int main(int argv, char** argc){

  // bdc, in this test we generate dataset(s) coming from
  // multiple sensors, each with a different time-delay, and
  // simultaneously estimate the sensor extrinsics and the time delays.

  // bdc, we call our first sensor "First-Fake-Sensor"
  std::string sensor1_name = "First-Fake-Sensor";
  // bdc, first sensor actual pose will be this one
  // x y z qx qy qz
  Vector6 sensor1_pose;
  sensor1_pose << 0.01, 0.1, 0.1, -0.25, 0.18, -0.68;
  Isometry3 sensor1_T = srrg_core::v2t(sensor1_pose);
  // bdc our first sensor has a certain delay (in sec.)
  float sensor1_time_delay = 0.12f;
  
  // bdc, we call our second sensor "Second-Fake-Sensor"
  std::string sensor2_name = "Second-Fake-Sensor";
  // bdc, second sensor actual pose will be this one
  // x y z qx qy qz
  Vector6 sensor2_pose;
  sensor2_pose << -0.01, 0.2, 0.05, -0.17, -0.25, 0.66;
  Isometry3 sensor2_T = srrg_core::v2t(sensor2_pose);
  // bdc our second sensor has a certain delay (in sec.)
  float sensor2_time_delay = -0.11f;

  // bdc, now our dataset
  const int data_size = 5000;
  // bdc, dataset sample rate
  float dt = 0.005;
  // bdc, let's generate every time something new
  srand (static_cast <unsigned> (time(NULL)));

  // bdc, we need an odometry dataset
  // i.e. the trajectory of our platform
  DatasetPtr odometry_dataset = DatasetPtr(new Dataset());
  // let's use a 3d bernoulli lemniscate (an eight-shape)
  bernoulliLemniscate(odometry_dataset, dt, 20);
  // bdc, a dataset for the our First Sensor
  DatasetPtr sensor1_dataset = DatasetPtr(new Dataset());
  // bdc, a dataset for the our Second Sensor
  DatasetPtr sensor2_dataset = DatasetPtr(new Dataset());

  // bdc, populate the sensor datasets
  for(const Sample& pair_time_meas : *odometry_dataset) {
    // bdc, the current timestamp
    const float ts = pair_time_meas.first;

    Isometry3 sensor1_iso = pair_time_meas.second->isometry() * sensor1_T;
    // bdc, apply some noise to the measurement
    applySomeNoise(sensor1_iso);
    MeasurePtr sensor1_measure_ptr(new Measure(sensor1_iso));
    // bdc, when inserting the sample in the dataset, we put a delayed timestamp
    sensor1_dataset->insert(Sample(ts-sensor1_time_delay,sensor1_measure_ptr));

    Isometry3 sensor2_iso = pair_time_meas.second->isometry() * sensor2_T;
    applySomeNoise(sensor2_iso);
    MeasurePtr sensor2_measure_ptr(new Measure(sensor2_iso));
    // bdc, when inserting the sample in the dataset, we put a delayed timestamp
    sensor2_dataset->insert(Sample(ts-sensor2_time_delay,sensor2_measure_ptr));
  }

  // bdc, now insert the datasets in a single map
  SensorDatasetMap sensor_dataset_map;
  sensor_dataset_map.insert(SensorDatasetPair(sensor1_name, sensor1_dataset));
  sensor_dataset_map.insert(SensorDatasetPair(sensor2_name, sensor2_dataset));
  
  std::cerr << "[Random Dataset Generated]!" << std::endl;
 
  // bdc, initialize a sensor with the same name used in the respective dataset
  SensorPtr sensor1 = SensorPtr( new Sensor(sensor1_name));
  // we want to estimate its time delay
  sensor1->setEstimateTimeDelay(true);
  // bdc, initialize our sensor with an identity as extrinsics
  sensor1->setExtrinsics(Vector6::Zero());
  // bdc, we can add a Prior on a parameter (that will keep that value fixed)
  // this is useful in case of non-observability of some param
  // as an example, say we are 100% sure that sensor1 X coord is 0.01
  sensor1->addPrior(0.01, Sensor::X);

  // bdc, let's do similar stuff for the secondo sensor
  SensorPtr sensor2 = SensorPtr(new Sensor(sensor2_name)); 
  sensor2->setEstimateTimeDelay(true);
  sensor2->setExtrinsics(Vector6::Zero());

  // bdc, print out our initial state
  std::cerr << "Initial State" << std::endl;
  sensor1->print();
  sensor2->print();
  
  // bdc, our solver needs to know the kinematics (if any, in this case we use directly
  // the odometry, so it does not need it) and the sensors (the pointers)
  Solver solver;
  solver.setVerbosity(true); 
  solver.setSensor(sensor1);
  solver.setSensor(sensor2);
  solver.setIterations(20);
  solver.setEpsilon(1e-4);
  // bdc, the epsilon used for the numeric jacobian of the time delay column
  // is highly dependant from the rate of the dataset
  solver.setEpsilonTime(dt);
  
  // bdc, always remember to initialize the solver
  solver.init();
  
  solver.compute(odometry_dataset, sensor_dataset_map);
  solver.stats().print();
  
  sensor1->print();
  std::cerr << "Actual Solution S1  : " << sensor1_pose.transpose() << std::endl;
  std::cerr << "Actual Time delay S1: " << sensor1_time_delay << std::endl;

  sensor2->print();
  std::cerr << "Actual Solution S2  : " << sensor2_pose.transpose() << std::endl;
  std::cerr << "Actual Time delay S2: " << sensor2_time_delay << std::endl;
    
  return 0;
}
