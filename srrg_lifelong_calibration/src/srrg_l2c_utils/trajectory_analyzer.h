#pragma once
#include <srrg_l2c_types/dataset.h>
#include "utils.h"

namespace srrg_l2c {

  class TrajectoryAnalyzer {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    TrajectoryAnalyzer();
    ~TrajectoryAnalyzer();

    struct Config {
      real translational_velocity_thresh = 0.01;
      real rotation_velocity_thresh = 0.2;
      real translational_acceleration_thresh = 1;
      real rotation_acceleration_thresh = 0.2;

      double observation_time = 0.3;
    };

    Config& mutableConfig() {return _config;}
    const Config& config() const {return _config;}
    
    void compute(Dataset& dataset_);

  protected:
    void getCurrentStatus(Measure::Status& status_,
                          const real& trans_vel_,
                          const real& rot_vel_,
                          const real& trans_acc_,
                          const real& rot_acc_);
    
  private:
    Config _config;

  };

}
