#include "trajectory_splitter.h"

namespace srrg_l2c {

  TrajectorySplitter::TrajectorySplitter(const Mode mode_) {
    _mode = mode_;
  }

  void TrajectorySplitter::setDataset(SensorDatasetMap& dataset_map_) {
    _dataset_map = dataset_map_;
  }

  void TrajectorySplitter::compute(SensorDatasetMapVector& dataset_vector_) {
    if(!_dataset_map.size())
      throw std::runtime_error("[TrajectorySplitter]: before calling compute, set a sensor dataset map");
    if(_mode == Mode::SampleNum)
      computeBySample(dataset_vector_);
    else if(_mode == Mode::Motion)
      computeByMotion(dataset_vector_);    
  }

  void TrajectorySplitter::getSensorPortion(Dataset& dataset_portion_,
                                            const Dataset& dataset_,
                                            const double& start,
                                            const double& end) {
    Dataset::const_iterator init = dataset_.lower_bound(start);
    for(; init != dataset_.end(); ++init) {
      if(end - init->first < 0)
        break;
      dataset_portion_.insert(Sample(init->first, init->second));
    }
  }

  bool TrajectorySplitter::getDatasetPortion(Dataset& dataset_portion,
                                             const Dataset& dataset,
                                             const double& start,
                                             const double& end) {
    Dataset::const_iterator init = dataset.lower_bound(start);
    for(; init != dataset.end(); ++init) {
      if(end - init->first < 0)
        return true;
      dataset_portion.insert(Sample(init->first, init->second));
    }
    return false;
  }
  
  void TrajectorySplitter::computeByMotion(SensorDatasetMapVector& dataset_map_vector_) {
    const double portion_time = (double)_config.sample_per_dataset / _config.rate;
    const double half_portion_time = portion_time / 2.f;
    
    DatasetPtr dataset = _dataset_map.begin()->second;
    TrajectoryAnalyzer trajectory_analyzer;
    trajectory_analyzer.compute(*dataset);
    
    std::vector<double> gauges;
    const int dataset_size = dataset->size();

    Measure::Status current_status = dataset->begin()->second->status();
    const double first_data_time = dataset->begin()->first;    
    Dataset::const_iterator it = dataset->end();
    it--;
    const double last_data_time = it->first;
    double init_time = first_data_time;
    // find gauge points
    for(const Sample& sample : *dataset) {      
      if(sample.second->status() != current_status) {
        double gauge_time = init_time + (sample.first - init_time) * 0.5f;
        gauges.push_back(gauge_time);
        init_time = sample.first;
        current_status = sample.second->status();
      }
    }

    // get portions from gauges
    dataset_map_vector_.resize(gauges.size());
    int current = 0;
    init_time = first_data_time;
    for(size_t i = 0; i < gauges.size(); ++i) {
      const double& gauge_time = gauges[i];
      const double current_init = gauge_time - half_portion_time;
      const double current_end  = gauge_time + half_portion_time;
      if(current_init > init_time &&
         current_end  < last_data_time) {
        SensorDatasetMap& sensor_dataset_map = dataset_map_vector_[current];
        for(const SensorDatasetPair& sensor_dataset : _dataset_map) {
          const std::string& sensor_name = sensor_dataset.first;
          const Dataset& dataset = *sensor_dataset.second;
          DatasetPtr portion = DatasetPtr(new Dataset);
          getSensorPortion(*portion, dataset, current_init, current_end);       
          sensor_dataset_map.insert(std::make_pair(sensor_name, portion));
        } 
        dataset_map_vector_[current++] = sensor_dataset_map;
        init_time = current_end;
      }
    }
    dataset_map_vector_.resize(current);
  }
  
  
  void TrajectorySplitter::computeBySample(SensorDatasetMapVector& dataset_map_vector_) {
    int id = 0;
    int number_of_samples = 0;

    DatasetVector dataset_vector;
    dataset_vector.clear();
    dataset_vector.push_back(DatasetPtr(new Dataset()));

    const DatasetPtr& dataset_ptr = _dataset_map.begin()->second;
    std::vector<double> times;
    
    for(const Sample& sample : *dataset_ptr) {
      if(!number_of_samples)
        times.push_back(sample.first);
      // dataset_vector[id]->insert(sample);
      ++number_of_samples;
      if(number_of_samples == _config.sample_per_dataset*(id+1)){
        ++id;
        times.push_back(sample.first);
        // dataset_vector.push_back(DatasetPtr(new Dataset()));
      }
    } 

    dataset_map_vector_.resize(times.size()-1);
    for(size_t i = 0; i < times.size()-1; ++i) {
      SensorDatasetMap& sensor_dataset_map = dataset_map_vector_[i];
      const double& start_time = times[i];
      const double& end_time   = times[i+1];
      for(const SensorDatasetPair& sensor_dataset : _dataset_map) {
        const std::string& sensor_name = sensor_dataset.first;
        const Dataset& dataset = *sensor_dataset.second;
        DatasetPtr portion = DatasetPtr(new Dataset);
        getSensorPortion(*portion, dataset, start_time, end_time);       
        sensor_dataset_map.insert(std::make_pair(sensor_name, portion));
      }      
    }
    
  }

}
