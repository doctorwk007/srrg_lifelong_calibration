#include "utils.h"

namespace srrg_l2c {

  void isoResize(Isometry2& out2_, const Isometry2& iso2_){
    out2_ = iso2_;
  }
  void isoResize(Isometry3& out3_, const Isometry3& iso3_){
    out3_ = iso3_;
  }
  void isoResize(Isometry2& out2_, const Isometry3& iso3_){
    out2_ = srrg_core::iso2(iso3_);
  }
  void isoResize(Isometry3& out3_, const Isometry2& iso2_){
    throw std::runtime_error("invalid conversion!");
  }

  real computeAngle(const Matrix3& R) {   
    //    # an invitation to 3-d vision, p 27
    real rotation_trace = R.trace();    
    return acos(std::min(real(1), std::max(real(-1), (rotation_trace - 1)/2)));
  }
  
  bool checkDistance(const Isometry3& relative_transform,
                     const real& translation_thresh_,
                     const real& rotation_thresh_) {
    if(relative_transform.translation().norm() > translation_thresh_)
      return true;
    if(computeAngle(relative_transform.rotation()) > rotation_thresh_)
      return true;
    return false;
  }


  std::string mangleStrings(const std::string& s1,
                            const std::string& s2) {
    return s1+"_x_"+s2;
  }

  bool unMangleStrings(std::string& s1,
                       std::string& s2,
                       const std::string& mangled) {

    std::size_t found = mangled.find("_x_");
    if (found==std::string::npos)
      return false;
      
    s1 = mangled.substr(0, found);
    s2 = mangled.substr(found+3);
    return true;
  }


  Isometry3 interpolate(const double& relative_time_,
                        const Isometry3& zero_time_isometry_,
                        const Isometry3& one_time_isometry_) {
    if(relative_time_ < 0.0 || relative_time_ > 1.0)
      throw std::runtime_error("[interpolate]: specify a time in the interval (0,1)");
    
    Vector7 zero_time = srrg_core::t2w(zero_time_isometry_);
    Quaternion zero_q(zero_time(6), zero_time(3), zero_time(4), zero_time(5));
    Vector7 one_time  = srrg_core::t2w(one_time_isometry_);
    Quaternion one_q(one_time(6), one_time(3), one_time(4), one_time(5));
    //interpolate quaternion
    zero_q.slerp(relative_time_, one_q);
    zero_q.normalize();
      
    //lerp
    Vector6 result = Vector6::Zero();
    result.head(3) = zero_time.head(3) * (1.0 - relative_time_);
    result.head(3) += relative_time_ * one_time.head(3);

    result(3) = zero_q.x();
    result(4) = zero_q.y();
    result(5) = zero_q.z();

    if(zero_q.w() < 0.0) 
      result.tail(3) *= -1;

    return srrg_core::v2t(result);    
  }

  VectorX interpolate(const double& relative_time_,
                      const VectorX& zero_time_encoder_ticks_,
                      const VectorX& one_time_encoder_ticks_) {
    if(relative_time_ < 0.0 || relative_time_ > 1.0)
      throw std::runtime_error("[interpolate]: specify a time in the interval (0,1)");
    //return one_time_encoder_ticks_;

    const int size = zero_time_encoder_ticks_.rows();

    for(size_t i = 0; i < size; ++i)
      if(zero_time_encoder_ticks_(i) * one_time_encoder_ticks_(i) < 0)
        return one_time_encoder_ticks_;
    
    // lerp
    VectorX result = zero_time_encoder_ticks_;
    result *= (1.0 - relative_time_);
    result += relative_time_ * (one_time_encoder_ticks_);
    
    for(size_t i = 0; i < size; ++i)
      srrg_core::normalizeAngle(result(i));

    // std::cerr << "0: " << zero_time_encoder_ticks_.transpose() << std::endl;
    // std::cerr << "1: " << one_time_encoder_ticks_.transpose() << std::endl;
    // std::cerr << "r: " << result.transpose() << std::endl;
    // std::cerr << "relative time: " << relative_time_ << std::endl << std::endl;

    return result;
  }

  
}
