#include <srrg_l2c_kinematics/differential_drive_kinematics.h>
#include <srrg_l2c_gui/dead_reckoning_viewer.h>
#include <srrg_l2c_ros/yaml_parser.h>

#include <srrg_l2c_ros/bag_loader.h>

#include <srrg_system_utils/system_utils.h>

using namespace srrg_l2c;

const char* banner [] = {
  "trajectory_viewer_app: from nw to valhalla",
  "",
  "usage: dead_reckoning_app -config <config-file> <bag-file>",
  "-config  <string>         configuration file",
  "-h       <flag>           this help",
  0
};


int main(int argc, char** argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  int c = 1;
  std::string config_file = "";
  std::string bag_file = "";
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-config")) {
      c++;
      config_file = argv[c];
    } else
      bag_file = argv[c];
    c++;
  }

  Matrix3 camera_matrix;
  camera_matrix << 285.17108154296875, 0.0, 160.0,
                   0.0, 285.1711120605469, 120.0,
                   0.0, 0.0, 1.0;
  
  SensorPtrVector sensors;
  YamlParser yaml_parser;
  yaml_parser.setYamlFile(config_file);
  yaml_parser.parse(sensors);

  std::string joint_topic = yaml_parser.jointTopic();
  std::cerr << "joint_topic : " << joint_topic << std::endl;
  std::string odom_topic  = yaml_parser.odomTopic();

  SensorPtr front_camera = sensors[0];
  front_camera->setCameraMatrix(camera_matrix);
  std::cerr << "using sensor: " << front_camera->name() << std::endl;

  
  DatasetPtr front_camera_dataset = std::make_shared<Dataset>();
  SensorDatasetMapPtr empty = std::make_shared<SensorDatasetMap>();
  empty->insert(std::make_pair(front_camera->name(), front_camera_dataset));

  DatasetPtr dataset = std::make_shared<Dataset>();

  std::cerr << "Loading bag" << std::endl;
  BagLoader bag_loader(bag_file);
  if(!joint_topic.empty())
    bag_loader.setJointTopic(joint_topic, yaml_parser.robotIntrinsics().size());
  else if(!odom_topic.empty())
    bag_loader.setOdomTopic(odom_topic);
  else
    throw std::runtime_error("[Main]: missing joint or odom topic");

  bag_loader.setImageTopic(front_camera->name(), "/camera_front/depth/image_raw");
  bag_loader.load(*dataset, *empty);
  std::cerr << "done" << std::endl;  

  DifferentialDriveKinematicsPtr dd_robot_ptr = DifferentialDriveKinematicsPtr(new DifferentialDriveKinematics());

  dd_robot_ptr->setOdomParams(yaml_parser.robotIntrinsics());  
  
  QApplication app(argc, argv);
  DeadReckoningViewer viewer;
  viewer.setDataset(dataset);
  viewer.setKinematics(dd_robot_ptr);
  viewer.setSensor(front_camera);
  viewer.setSensorDataset(empty);
  
  viewer.compute();
  
  viewer.show();
  
  return app.exec();
  
}
