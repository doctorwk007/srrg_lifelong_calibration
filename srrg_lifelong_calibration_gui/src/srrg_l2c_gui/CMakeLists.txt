add_library(srrg_l2c_gui_library SHARED
  trajectory_viewer.cpp
  dead_reckoning_viewer.cpp
  )

target_link_libraries(srrg_l2c_gui_library
  ${catkin_LIBRARIES}
  ${QGLVIEWER_LIBRARY} 
  ${SRRG_QT_LIBRARIES} 
  ${OPENGL_gl_LIBRARY} 
  ${OPENGL_glu_LIBRARY}
  ${OpenCV_LIBS}
  )

