#include "frame_handler.h"

namespace srrg_l2c {

  FramesHandle::FramesHandle(){
    name = "";
    frame_id = "";
    child_frame_id = "";
    old_iso.setIdentity();
    current_iso.setIdentity();
  }
  FramesHandle::FramesHandle(const std::string& name_,
                             const std::string& frame_id_,
                             const std::string& child_frame_id_){
    name = name_;
    frame_id = frame_id_;
    child_frame_id = child_frame_id_;
    old_iso.setIdentity();
    current_iso.setIdentity();
  } 
  FramesHandle::FramesHandle(const FramesHandle& c){
    name = c.name;
    frame_id = c.frame_id;
    child_frame_id = c.child_frame_id;
    old_iso = c.old_iso;
    current_iso = c.current_iso;
  }
  
  const void FramesHandle::print() const {
    std::cerr << "name:           " << name << std::endl;
    std::cerr << "frame_id:       " << frame_id << std::endl;
    std::cerr << "child_frame_id: " << child_frame_id << std::endl;
  }
  
}
